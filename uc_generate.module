<?php

/**
 * @file
 * Generate random products.
 * 
 * Coded by cha0s <rubentbstk@gmail.com>
 * for client Prima Supply <http://www.primasupply.com>
 */

/**
 * Implementation of hook_menu().
 */
function uc_generate_menu() {
  $items = array();

  if (module_exists('uc_attribute')) {
    $items['admin/generate/uc-attribute'] = array(
      'title' => 'Generate Ubercart attributes',
      'description' => 'Generate a given number of attributes. Optionally delete current attributes.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('uc_generate_attributes_form'),
      'access arguments' => array('administer attributes'),
    );
  }

  return $items;
}

function uc_generate_attributes_form() {
  $form['num'] = array(
    '#type' => 'textfield',
    '#title' => t('How many attributes would you like to generate?'),
    '#default_value' => 10,
    '#size' => 10,
  );
  $form['kill_attributes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete all attributes before generating.'),
    '#default_value' => FALSE,
  );

  $form['max_options'] = array(
    '#type' => 'textfield',
    '#title' => t('How many options per attribute, max?'),
    '#default_value' => 5,
    '#size' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Do it!'),
  );
  return $form;
}

function uc_generate_attributes_form_submit($form_id, &$form_state) {
  uc_generate_create_uc_attributes($form_state['values']['num'], $form_state['values']['kill_attributes'], $form_state['values']['max_options']);
  drupal_set_message(t("@num attributes created.", array('@num' => $form_state['values']['num'])));
}

function uc_generate_create_uc_attributes($num, $kill, $max) {
  // For dee greeking
  module_load_include('inc', 'devel_generate');
  
  // Kill existing attributes first?
  if ($kill) {
    // Get all of em.
    $result = db_query("SELECT aid FROM {uc_attributes}");
    while ($aid = db_result($result)) {
      // Load it just for the name.
      $attribute = uc_attribute_load($aid);
            
      // Delete it.
      uc_attribute_delete($aid);
      
      // Echo!
      drupal_set_message(t("Deleted the @name attribute and all its options.", array('@name' => $attribute->name)));
    }
  }
  
  // Generate $num attributes.
  for ($i = 0; $i < $num; $i++) {
    $attribute = array(
      'name' => devel_create_greeking(rand(1, 5), TRUE),
      'label' => devel_create_greeking(rand(1, 5), TRUE),
      'description' => devel_create_greeking(rand(1, 5)),
      'required' => rand(0, 1) ? TRUE : FALSE,
      'display' => rand(0, 3),
      'ordering' => rand(-20, 20),
    );
    uc_attribute_save($attribute);
    
    // Generate between 1 and $max options per attribute.
    for ($j = 0; $j < rand(1, $max); $j++) {
      $option = array(
        'aid' => $attribute['aid'],
        'name' => devel_create_greeking(rand(1, 5), TRUE),
        'ordering' => rand(-20, 20),
        'cost' => rand(0, 250),
        'price' => rand(0, 250),
        'weight' => rand(0, 250),
      );
      uc_attribute_option_save($option);
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function uc_generate_nodeapi(&$node, $op) {
  // We're only interested in devel generate'd nodes.
  if (!isset($node->devel_generate)) {
    return;
  }
  
  // Seed the random number generator.
  srand((double) microtime()*1000000);
  
  switch ($op) {
    
    case 'presave':
      // Generate random product data. This could be borrowed from the unit tests
      // I wrote if they get into core. $duplication--;
      $weight_types = array('lb', 'kg', 'oz', 'g');
      $length_types = array('in', 'ft', 'cm', 'mm');
      
      $node->model = devel_generate_word(16);
      $node->list_price = rand(.01, 1000);
      $node->cost = rand(.01, 1000);
      $node->sell_price = rand(.01, 1000);
      $node->weight = rand(1, 50);
      $node->length = rand(1, 50);
      $node->height = rand(1, 50);
      $node->weight_units = $weight_types[array_rand($weight_types)];
      $node->length_units = $length_types[array_rand($length_types)];
      $node->pkg_qty = rand(1, 30);
      $node->default_qty = rand(1, 5);
      $node->ordering = rand(-20, 20);
      $node->shippable = rand(0, 1);
      
    break;
    
    case 'insert':
      
      if (module_exists('uc_attribute')) {
        // 0 or more attributes.
        $count = db_result(db_query("SELECT COUNT(*) FROM {uc_attributes}"));
        $result = db_query("SELECT aid FROM {uc_attributes}");
        while ($aid = db_result($result)) {
          // Attach around half of the available options to this product.
          if (rand(0, $count / 2) != 0) continue;
          
          // Attach the attributes.
          $attribute = uc_attribute_load($aid);
          uc_attribute_subject_save($attribute, 'product', $node->nid, TRUE);
        }
      }
      
    break;
  } 
}
